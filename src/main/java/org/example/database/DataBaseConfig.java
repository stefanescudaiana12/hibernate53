package org.example.database;

import org.example.ex2.Student;
import org.example.relations.entity.*;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class DataBaseConfig {
    private static SessionFactory sessionFactory = null;

    private DataBaseConfig() {
    }

    public static SessionFactory getSessionFactory() {
        if(sessionFactory == null){
            sessionFactory = new Configuration()
                    .configure("Hibernate.config.xml")
                    .addAnnotatedClass(Student.class)
                    .addAnnotatedClass(Child.class)
                    .addAnnotatedClass(Food.class)
                    .addAnnotatedClass(Hobby.class)
                    .addAnnotatedClass(Job.class)
                    .addAnnotatedClass(Mother.class)
                    .addAnnotatedClass(Toy.class)
                    .addAnnotatedClass(TvShow.class)
                    .addAnnotatedClass(Animal.class)
                    .addAnnotatedClass(Owner.class)
                    .buildSessionFactory();
        }
        return sessionFactory;
    }
}
