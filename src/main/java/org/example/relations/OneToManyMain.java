package org.example.relations;

import org.example.database.DataBaseConfig;
import org.example.ex1.Genre;
import org.example.relations.entity.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class OneToManyMain {
    public static void main(String[] args) {
//            SessionFactory sessionFactory = new Configuration()
//                    .configure("Hibernate.config.xml")
//                    .addAnnotatedClass(Mother.class)
//                    .addAnnotatedClass(Hobby.class)
//                    .buildSessionFactory();
            SessionFactory sessionFactory = DataBaseConfig.getSessionFactory();

            Session session =sessionFactory.openSession();
            Transaction transaction = session.beginTransaction();

            Hobby hobby1 = new Hobby(null, "Tennis", "beginner");
            Hobby hobby2 = new Hobby(null, "Choss", "intermediate");
            TvShow tvShow = new TvShow(null,"TvShows", Genre.COMEDY);
            TvShow tvShow2 = new TvShow(null,"TvShows2", Genre.ACTION);
            //nu vrem sa le facem persist pentru ca operatia de persist este cascadata din mother
            Mother mother1 = new Mother(null,"Ioana", Job.PROFESOR, List.of(hobby1, hobby2), List.of(tvShow, tvShow2));

            session.persist(hobby1);
            session.persist(hobby2);
            session.persist(mother1);


            transaction.commit();
            session.close();

    }
}
