package org.example.relations;

import org.example.database.DataBaseConfig;
import org.example.relations.entity.Child;
import org.example.relations.entity.Food;
import org.example.relations.entity.Mother;
import org.example.relations.entity.Toy;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class OneToOneMain {
    public static void main(String[] args) {
        //creare la baza de date:
//        SessionFactory sessionFactory = new Configuration()
//                .configure("Hibernate.config.xml")
//                .addAnnotatedClass(Child.class)
//                .addAnnotatedClass(Mother.class)
//                .addAnnotatedClass(Food.class)
//                .addAnnotatedClass(Toy.class)
//                .buildSessionFactory();

        SessionFactory sessionFactory = DataBaseConfig.getSessionFactory();

        Session session =sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        Food f1 = new Food(1,"milk", true);
        session.persist(f1);
        Toy toy1 = new Toy(1,"Toy");
        Child c1= new Child(1, "Johny", f1, toy1);
        session.persist(c1);
        transaction.commit();
        System.out.println("Child was saved");

        Transaction transaction2 = session.beginTransaction();
        session.remove(c1);
        transaction2.commit();
        System.out.println("Child was removed");
        System.out.println("Check what happend with toy");
        session.close();
    }
}
