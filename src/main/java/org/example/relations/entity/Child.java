package org.example.relations.entity;

import jakarta.persistence.*;

@Entity
@Table(name = "children")

public class Child {
    @Id // primary key

    private Integer id;
    private String name;
    //presupun ca fiecare copil are o singura mancare favorita
    //si presupun ca nu exista 2 copii care sa aiba aceeasi mancare favorita
    //fiecare mancare este favorita unui singur copil
    @OneToOne //foreign key
    private Food favoriteFood;
    @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.REMOVE})//nu trebuie creat in baza de date cu session.persist pentru ca avem cascade

    private Toy favoriteToy;

    public Child(Integer id, String name, Food favoriteFood, Toy favoriteToy) {
        this.id = id;
        this.name = name;
        this.favoriteFood = favoriteFood;
        this.favoriteToy = favoriteToy;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Food getFavoriteFood() {
        return favoriteFood;
    }

    public void setFavoriteFood(Food favoriteFood) {
        this.favoriteFood = favoriteFood;
    }

    public Toy getFavoriteToy() {
        return favoriteToy;
    }

    public void setFavoriteToy(Toy favoriteToy) {
        this.favoriteToy = favoriteToy;
    }
}
